process.env.TARGETRON_SERVER_MODE = 'test';
let Database = require('../src/model/Database.js');
let Targets = require('../src/model/Target.js').Targets;
let Target = require('../src/model/Target.js').Target;

exports.testDatabaseConnection = async function(test) {
    test.expect(1);
    try {
        let sql = 'SELECT 1';
        let results = await Database.query(sql);
        test.ok(results.length > 0, "Should return 1 if database exists")
    } catch(e) {
        test.ok(false, "Database threw exception: " + e.message);
    }

    test.done();
};

exports.testTargetRegister = async function(test) {
    test.expect(3);

    try {
        let macAddress = "AB:CD:EF:FE:DC:BA";
        let ipAddress = "192.168.1.1";
        let target = await Targets.register(macAddress, ipAddress);
        test.ok(target != null, "Target not null");
        test.ok(target.id != null, "Target created successfully");

        let target2 = await Targets.getByMacAddress(macAddress);
        test.ok(target2.id === target.id, "Target retrieved from database");
    } catch(e) {
        test.ok(false, "Targets threw an exception: " + e.message);
    }

    test.done();
};

exports.testGetByMacAddress = async function(test) {
    test.expect(1);

    try {
        let target = new Target(null, "abc123", "1.1.1.1");
        target = await target.save();

        let target2 = await Targets.getByMacAddress("abc123");
        test.ok(target2.id === target.id, "Target retrieved from database");
    } catch(e) {
        test.ok(false, "Targets threw an exception: " + e.message);
    }

    test.done();
};

exports.setUp = async function(callback) {
    try {
        await Targets.dropTable();
        await Targets.createTable();
    } catch(e) {
        console.log(e);
    }
    callback();
};

 exports.tearDown = async function(callback) {
     await Database.close();
     callback();
};



// exports.tearDown = function() {
//     // Database.close();
// };
