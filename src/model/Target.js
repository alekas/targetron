let WebSocket = require('ws');
let events = require('events');
let EventEmitter = events.EventEmitter;

class Target extends EventEmitter {

    constructor(id, ipAddress, port) {
        super();
        this._id = id;
        this._ipAddress = ipAddress;
        this._port = port;
    }

    connect() {
        let that = this;
        return new Promise(function(resolve, reject) {

            if(that.ipAddress) {
                that.client = new WebSocket('ws://' + that.ipAddress + ':' + that._port + '/');
                that.client.on('open', that.onConnect.bind(that));
                that.client.on('error', that.onError.bind(that));
                that.client.on('message', that.onMessage.bind(that));
            }

            that.client.on('open', function() {
                resolve(that.client);
            });
            that.client.on('error', function(err) {
                reject(that.client);
            });

        });
    }

    onMessage(message) {
        if(message === 'HIT') {
            this.emit('hit', this);
        } else {
            this.emit('messageReceived', message);
        }
    }

    onError(error) {
        this.emit('error', error);
        console.log(error);
    }

    onConnect(webSocketConnection) {
        this.emit('connect', webSocketConnection);
        console.log('Connected to ' + this.ipAddress);
    }

    setColor(color) {
        this.client.send(color);
    }

    fade(color) {
        let command = 'F' + color.substr(1);
        this.client.send(command);
    }

    playLongTone(pitch) {
        this.client.send('L' + pitch);
    }

    playShortTone(pitch) {
        this.client.send('S' + pitch);
    }

    get ipAddress() {
        return this._ipAddress;
    }

    set ipAddress(value) {
        this._ipAddress = value;
    }


    get port() {
        return this._port;
    }

    set port(value) {
        this._port = value;
    }


    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }
}

exports.Target = Target;