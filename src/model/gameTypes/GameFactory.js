class GameFactory {
    constructor() {
        GameFactory._instance = null;
        GameFactory.games = [
            {
                id: "timeAttack",
                name: "Time Attack",
                maxPlayers: 1
            }
        ];
    }

    static all() {
        if(!GameFactory._instance) {
            GameFactory._instance = new GameFactory();
        }

        return GameFactory.games;
    }
}

exports.GameFactory = GameFactory;