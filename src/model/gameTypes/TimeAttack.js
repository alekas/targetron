let Game = require("../Game").Game;
let moment = require("moment")
let User = require("../User").User;

class TimeAttack extends Game {
    constructor(duration) {
        super();
        this.type = "TimeAttack";
        this.duration = duration;
        this.interval = null;
        this.targetSwapTime = null;
    }
    processHit(target) {
        let hit = super.processHit(target);
        if(hit.targetId !== this.activeTarget.id) {
            return;
        }

        console.log("Target " + target.id + " was hit!");
        this.hits.push(hit);
    }

    run() {
        let that = this;
        //
        // let now = moment();
        // let second1 = now.clone().add(1.5, 's');
        // let second2 = now.clone().add(3, 's');
        // let second3 = now.clone().add(4.5, 's');
        //
        //
        // this.broadcastTargetColor('#ff0000');
        // this.broadcastShortTone(587);
        // while(second1.isAfter(moment())) {
        //     //block
        // }
        //
        //
        // this.broadcastTargetColor('#ffff00');
        // this.broadcastShortTone(587);
        // while(second2.isAfter(moment())) {
        //     //block
        // }
        //
        //
        // this.broadcastTargetColor('#ffff00');
        // this.broadcastShortTone(587);
        // while(second3.isAfter(moment())) {
        //     //block
        // }
        //
        // this.broadcastTargetColor('#00ff00');
        // this.broadcastLongTone(880);

        this.timeStarted = moment();

        let endTime = this.timeStarted.clone();
        endTime.add(this.duration, 's');
        this.broadcastTargetColor('#ff0000');
        this.setActiveTarget(this.getRandomTarget());
        this.isRunning = true;

        this.interval = setInterval(async function() {

            if(that.targetSwapTime && that.targetSwapTime.isBefore(moment())) {
                that.swapActiveTarget();
            }
            if(that.targetSwapTime === null || that.targetSwapTime.isBefore(moment())) {
                that.targetSwapTime = moment().add(Math.floor(Math.random() * 3) + 5, 's');

                console.log("Setting targetSwap time to " + that.targetSwapTime.format('hh:mm:ss') + ", Current time: " + moment().format('hh:mm:ss'));
            }

            if(endTime.isBefore(moment())) {
                await that.gameOver();
            }
        }, 100);
    }

    swapActiveTarget() {
        let nextTarget = this.activeTarget;
        while(nextTarget === this.activeTarget) {
            nextTarget = this.getRandomTarget();
        }

        this.setActiveTarget(nextTarget);
    }

    setActiveTarget(target) {
        if(this.activeTarget) {
            this.activeTarget.setColor('#ff0000');
        }
        this.activeTarget = target;
        this.activeTarget.setColor('#00ff00');

    }

    async gameOver() {
        this.players[0].score = this.hits.length;
        await super.gameOver();
        this.broadcastFade("#0000ff");
        console.log("Game Over! There were " + this.hits.length + " in " + this.duration + " seconds");
        this.isRunning = false;
        clearInterval(this.interval);

        this.endGame();
    }
}

/**
 * usage: node TimeAttack.js -d duration -u userId
 */
(async function() {
    let duration = 60;
    let userId = 1;
    // let argv = process.argv;
    // for(let i = 0; i < process.argv.length; i++) {
    //     if(argv[i] === '-d') {
    //         duration = argv[i+1];
    //     }
    //     if(argv[i] === '-u') {
    //         userId = argv[i+1];
    //     }
    // }
    //
    // if(duration === null) {
    //     console.log("Duration is required. Use the -d switch to specify duration");
    //     process.exit(1);
    // }
    //
    // if(userId === null) {
    //     console.log("UserId is required. Use the -u switch to specify user");
    //     process.exit(1);
    // }

    let game = new TimeAttack(duration);
    game.addPlayer(new User(userId));
    console.log("Connecting to targets...");
    await game.prepare();
    game.run();

})();

exports.TimeAttack = TimeAttack;