
class Player {

    constructor(id, user, gameId) {
        this._id = id;
        this._user = user;
        this._score = 0;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }
    get score() {
        return this._score;
    }

    set score(value) {
        this._score = value;
    }

    get user() {
        return this._user;
    }

    set user(value) {
        this._user = value;
    }
}

exports.Player = Player;