
class Hit {
    constructor(timestamp, userId, gameId, targetId) {
        this.timestamp = timestamp;
        this.userId = userId;
        this.gameId = gameId;
        this.targetId = targetId;
    }
}

exports.Hit = Hit;