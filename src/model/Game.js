let http = require("http");
let moment = require('moment');
let Target = require('./Target.js').Target;
let Hit = require('./Hit').Hit;
let Player = require('../model/Player').Player;
let request = require('request-promise-native');
let parser = require('fast-xml-parser');
let ssdp = require('node-ssdp').Client

class Game {

    constructor(id, type, timeStarted, timeEnded) {
        this.id = id;
        this._type = type;
        this._timeStarted = timeStarted;
        this._timeEnded = timeEnded;
        this.isRunning = false;
        this.hits = [];
        this.targets = [];
        this.players = [];
        this.ssdpClient = new ssdp({});
        this.targetSearchTimeout = 5000;
    }


    findTargets() {
        let self = this;
        return new Promise(function(resolve, reject) {
            self.ssdpClient.on('notify', function () {
                console.log('Got a notification.')
            });

            self.ssdpClient.on('response', async function inResponse(headers, code, rinfo) {
                console.log('Got a response to an m-search:\n%d\n%s\n%s', code, JSON.stringify(headers, null, '  '), JSON.stringify(rinfo, null, '  '));

                let descriptionXml = await request(headers.LOCATION);
                if(parser.validate(descriptionXml) === true) {
                    let descriptionJson = parser.parse(descriptionXml);
                    console.log(descriptionJson);
                    let udn = descriptionJson.root.device.UDN;
                    self.addTarget(udn.substr(5), rinfo.address, descriptionJson.root.device.websocketPort);
                }
            });

            self.ssdpClient.search('urn:iventuresolutions:device:target:1');

            setTimeout(function() {resolve()}, self.targetSearchTimeout);
        });



// Or maybe if you want to scour for everything after 5 seconds
//         setInterval(function() {
//             client.search('ssdp:all')
//         }, 5000)
    }

    addTarget(id, ipAddress, port) {
        let target = new Target(id, ipAddress, port);
        let found = false;
        for(let i = 0; i < this.targets.length; i++) {
            if(this.targets[i].ipAddress === ipAddress && this.targets[i].port === port) {
                found = true;
                break;
            }
        }

        if(!found) {
            this.targets.push(target);
        }
    }

    addPlayer(user) {
        let player = new Player(null, user, this.id);
        this.players.push(player);
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get userId() {
        return this._userId;
    }

    set userId(value) {
        this._userId = value;
    }

    get score() {
        return this._score;
    }

    set score(value) {
        this._score = value;
    }

    get type() {
        return this._type;
    }

    set type(value) {
        this._type = value;
    }

    get timeStarted() {
        return this._timeStarted;
    }

    set timeStarted(value) {
        this._timeStarted = value;
    }

    get timeEnded() {
        return this._timeEnded;
    }

    set timeEnded(value) {
        this._timeEnded = value;
    }

    async gameOver() {
        this.timeEnded = moment();
        // await this.save();
    }

    async connectToTargets() {
        // this.targets = await Targets.getAll();
        for(let i = 0; i < this.targets.length; i++) {
            await this.targets[i].connect();
            this.targets[i].on('hit', this.processHit.bind(this));
        }
    }

    processHit(target) {
        if(!this.isRunning) {
            return;
        }
        return new Hit(moment(), 1, this.id, target.id);
    }

    async prepare() {
        await this.findTargets();
        await this.connectToTargets();
    }

    broadcastTargetColor(color) {
        for(let i = 0; i < this.targets.length; i++) {
            this.targets[i].setColor(color);
        }
    }

    broadcastFade(color) {
        for(let i = 0; i < this.targets.length; i++) {
            this.targets[i].fade(color);
        }
    }

    broadcastShortTone(pitch) {
        for(let i = 0; i < this.targets.length; i++) {
            this.targets[i].playShortTone(pitch);
        }
    }

    broadcastLongTone(pitch) {
        for(let i = 0; i < this.targets.length; i++) {
            this.targets[i].playLongTone(pitch);
        }
    }

    getRandomTarget() {
        return this.targets[Math.floor(Math.random() * this.targets.length)];
    }

    /**
     * Game loop. Overridden by subclasses
     */
    run() {
    }

    endGame() {
        process.exit(0);
    }
}

exports.Game = Game;