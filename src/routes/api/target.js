let express = require('express');
let Target = require('../../model/Target.js').Target;
let Game = require('../../model/Game').Game;
let Games = require('../../model/Game').Games;
let moment = require('moment');

let router = express.Router();


router.post('/register', async function(req, res, next) {
    let body = JSON.parse(req.rawBody);

    let target = await Targets.register(body.macAddress, body.ipAddress);
});

module.exports = router;
