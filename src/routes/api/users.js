let express = require('express');
let User = require('../../model/User.js').User;
let moment = require('moment');

let router = express.Router();


router.get('/', async function(req, res, next) {
    let users = [
        new User(1, 'Andreas', 'Lekas'),
        new User(2, 'Ismael', 'Delgado'),
        new User(3, 'Scott', 'Thompson'),
    ]
    res.send(users);
});

module.exports = router;
