var express = require('express');
var router = express.Router();
let GameFactory = require('../model/gameTypes/GameFactory').GameFactory;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', games: GameFactory.all()});
});

module.exports = router;
